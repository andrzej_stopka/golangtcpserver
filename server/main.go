package main

import (
	"fmt"
	"log"
	"net"
	"server/database"
	"server/server"
	"time"
)

var welcomeMessage string = "Welcome to TCP Server"

func main() {
	server := server.Server{WelcomeMessage: welcomeMessage}
	err := server.GetConfig()
	if err != nil {
		log.Fatalf("Error getting config: %v", err)
	}
	server.Database, err = database.Init(server.Config.Database.DSN, server.Config.Elastic.Url)
	if err != nil {
		log.Fatalf("Error initializing database: %v", err)
	}

	server.StartTime = time.Now()
	log.Printf("Server has been started at %s", server.StartTime.Round(time.Second).String())
	ln, err := net.Listen("tcp", fmt.Sprintf("%s:%s", server.Config.Server.Host, server.Config.Server.Port))
	if err != nil {
		log.Fatalf("error listening for connections: %v", err)
		return
	}

	for {
		conn, err := ln.Accept()
		if err != nil {
			log.Fatalf("error accepting connection: %v", err)
			return
		}
	
		log.Printf("Accepting connection from %s", conn.RemoteAddr())
	
		go func(c net.Conn) {
			stop := server.HandleConnection(c)
			c.Close()
			if stop {
				ln.Close()
				log.Print("Server has been closed")
			}
		}(conn)
	}
}
