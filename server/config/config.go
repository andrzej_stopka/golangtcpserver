package config

import (
	"fmt"
	"github.com/spf13/viper"
)

type Config struct {
	Server struct {
		Host           string
		Port           string
		AdminRecipient string `mapstructure:"admin_recipient"`
		AdminPassword  string `mapstructure:"admin_password"`
	}

	Database struct {
		User     string
		Password string
		Host     string
		Port     string
		Dbname   string
		DSN      string
	}

	Elastic struct {
		Url string
	}
}

func GetConfig() (*Config, error) {
	viper.SetConfigName("config")
	viper.SetConfigType("ini")
	viper.AddConfigPath("./config")

	var config Config

	if err := viper.ReadInConfig(); err != nil {
		return nil, fmt.Errorf("error reading config file: %v", err)
	}

	err := viper.Unmarshal(&config)
	if err != nil {
		return nil, fmt.Errorf("unable to decode into struct: %v", err)
	}

	config.Database.DSN = fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable",
		config.Database.User,
		config.Database.Password,
		config.Database.Host,
		config.Database.Port,
		config.Database.Dbname)
	return &config, nil
}
