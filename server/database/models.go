package database

import (
	"time"
)

type User struct {
	ID        uint      `gorm:"primaryKey" json:"-"`
	Username  string    `gorm:"unique;not null" json:"username"`
	Password  string    `gorm:"not null" json:"password"`
	Admin     bool      `gorm:"not null" json:"-"`
	CreatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"-"`
}

type Message struct {
	ID          uint      `gorm:"primaryKey"`
	SenderID    uint      `gorm:"not null"`
	RecipientID uint      `gorm:"not null"`
	Message     string    `gorm:"not null"`
	CreatedAt   time.Time `gorm:"default:CURRENT_TIMESTAMP"`
}
