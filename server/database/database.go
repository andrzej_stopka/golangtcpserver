package database

import (
	"context"
	"github.com/olivere/elastic/v7"
	"fmt"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"encoding/json"
)

type Database struct {
	db *gorm.DB
	elastic    *elastic.Client
}

func Init(dbURL, elasticURL string) (*Database, error) {
	db, err := gorm.Open(postgres.Open(dbURL), &gorm.Config{})
	if err != nil {
		return nil, err
	}

	err = db.AutoMigrate(&User{}, &Message{})
	if err != nil {
		return nil, err
	}

	elastic, err := elastic.NewClient(
		elastic.SetURL(elasticURL),
		elastic.SetSniff(false),
	)
	if err != nil {
		return nil, err
	}
	database := &Database{db: db, elastic: elastic}
	err = database.MigrateMessagesToElastic()
	if err != nil {
		return nil, err
	}

	return &Database{db: db, elastic: elastic}, nil
}

func (db *Database) MigrateMessagesToElastic() error {
	var messages []Message
	err := db.db.Find(&messages).Error
	if err != nil {
		return fmt.Errorf("failed to fetch messages from Postgres: %v", err)
	}

	for _, message := range messages {
		_, err := db.elastic.Index().
			Index("messages").
			Id(fmt.Sprintf("%d", message.ID)).
			BodyJson(message).
			Do(context.Background())

		if err != nil {
			return fmt.Errorf("failed to index message in ElasticSearch: %v", err)
		}
	}
	return nil
}

func (db *Database) Register(user User) error {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	if err != nil {
		return err
	}
	user.Password = string(hashedPassword)
	err = db.db.Create(&user).Error
	return err
}

func (db *Database) Login(user User) error {
	var registerUser User
	err := db.db.Where("username = ?", user.Username).First(&registerUser).Error
	if err != nil {
		return err
	}

	err = bcrypt.CompareHashAndPassword([]byte(registerUser.Password), []byte(user.Password))
	if err != nil {
		return err
	}
	return nil
}

func (db *Database) ResetPassword(user User, newPassword string) error {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(newPassword), bcrypt.DefaultCost)
	if err != nil {
		return err
	}
	err = db.db.Model(&User{}).Where("username = ?", user.Username).Update("password", hashedPassword).Error
	return err
}

func (db *Database) SendMessage(message Message) error {
	err := db.db.Create(&message).Error
	if err != nil {
	  return err
	}
	_, err = db.elastic.Index().Index("messages").Id(fmt.Sprintf("%d", message.ID)).BodyJson(message).Do(context.Background())
	return err
}

func (db *Database) SendMessageToAll(message Message) error {
	var users []User
	err := db.db.Find(&users).Error
	if err != nil {
		return err
	}

	for _, user := range users {
		newMessage := Message{
			SenderID:    message.SenderID,
			RecipientID: user.ID,
			Message:     message.Message,
			CreatedAt:   message.CreatedAt,
		}
		
		err = db.SendMessage(newMessage)
		if err != nil {
			return err
		}
	}

	return nil
}

func (db *Database) ReadMessages(user User) ([]Message, error) {
	var messages []Message
	err := db.db.Where("recipient_id = ?", user.ID).Find(&messages).Error
	return messages, err
}

func (db *Database) ClearInbox(user User) error {
	var messages []Message
	err := db.db.Where("recipient_id = ?", user.ID).Find(&messages).Error
	if err != nil {
		return err
	}

	for _, message := range messages {
		err := db.db.Delete(&message).Error
		if err != nil {
			return err
		}

		_, err = db.elastic.Delete().
			Index("messages").
			Id(fmt.Sprintf("%d", message.ID)).
			Do(context.Background())
		if err != nil {
			return err
		}
	}
	return nil
}

func (db *Database) GrantAdminAccess(user User) error {
	err := db.db.Model(&User{}).Where("username = ?", user.Username).Update("admin", true).Error
	return err
}

func (db *Database) DeleteUser(user User) error {
	err := db.db.Delete(&user).Error
	if err != nil {
		return err
	}
	return nil
}

func (db *Database) UserIsAdmin(userId uint) (bool, error) {
	var user User
	err := db.db.Model(&User{}).Where("id = ?", userId).First(&user).Error
	if err != nil {
		return false, err
	}
	return user.Admin, nil
}

func (db *Database) GetUserIDByUsername(username string) (uint, error) {
	var user User
	err := db.db.Model(&User{}).Where("username = ?", username).First(&user).Error
	if err != nil {
		return 0, err
	}
	return user.ID, nil
}

func (db *Database) GetUsernameByUserID(userId uint) (string, error) {
	var user User
	err := db.db.Model(&User{}).Where("id = ?", userId).First(&user).Error
	if err != nil {
		return "", err
	}
	return user.Username, nil
}

func (db *Database) GetUserByUsername(username string) (User, error) {
	var user User
	err := db.db.Model(&User{}).Where("username = ?", username).First(&user).Error
	if err != nil {
		return User{}, err
	}
	return user, nil
}

func (db *Database) CountMessages(userId uint) (int64, error) {
	var count int64
	err := db.db.Model(&Message{}).Where("recipient_id = ?", userId).Count(&count).Error
	return count, err
}

func (db *Database) FindMessages(query string, userID uint) ([]Message, error) {
	searchResult, err := db.elastic.Search().
		Index("messages"). 
		Query(elastic.NewBoolQuery().
			Must(elastic.NewMatchQuery("Message", query)).
			Filter(elastic.NewTermQuery("RecipientID", userID))).
		Do(context.Background())

	if err != nil {
		return nil, err
	}

	var messages []Message
	for _, hit := range searchResult.Hits.Hits {
		var message Message
		err := json.Unmarshal(hit.Source, &message)
		if err != nil {
			return nil, err
		}
		messages = append(messages, message)
	}

	return messages, nil
}
