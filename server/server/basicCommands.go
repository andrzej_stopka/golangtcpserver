package server

import (
	"time"
	"strings"
)

func (s *Server) GetUptime(serverResponse *ServerResponse) {
	serverResponse.Response = time.Since(s.StartTime).Round(time.Second).String()
}

func (s *Server) GetInfo(serverResponse *ServerResponse) {
	serverResponse.Response = `[TITLE] - Golang TCP Client/Server @Andrzej Stopka
[VERSION] - 1.0.0
[DATE CREATED] - 21.05.2024`
}

func (s *Server) GetHelp(clientResponse *ClientResponse, serverResponse *ServerResponse) {
	var response string
	clientResponse.User.Username = strings.ToLower(clientResponse.User.Username)
	if success, _ := s.checkAdminPrivileges(clientResponse.User.Username); success {
		response += `[SEND MESSAGES TO ALL USERS] - sendall
[READ USER INBOX] - readfor
[DELETE USER ACCOUNT] - delete
`
	}
	if clientResponse.Logged {
		response += `[SEND MESSAGE] - send
[READ YOUR INBOX] - read
[LOGOUT] - logout
`
	} else {
		response += `[REGISTER USER ACCOUNT] - register
[LOGIN] - login
`
	}
	response += `[RESET YOUR PASSWORD] - reset
[SHOW THE SERVER'S UPTIME] - uptime
[SHOW THE SERVER'S INFO] - info
[SHOW ALL AVAILABLE COMMANDS] - help
[STOP THE SERVER] - stop`
	serverResponse.Response = response
}
