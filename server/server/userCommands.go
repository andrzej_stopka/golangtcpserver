package server

import (
	"fmt"
	"server/database"
	"strings"
	"time"
)

var userIsNotLoggedMessage = "You are not logged in, please login first and try again"
var userIsLoggedMessage = "You are already logged in, please log out first and try again"
var recipientInboxIsFullMessage = "Recipient inbox is full, you cannot send messages to recipients that have 5 or more messages"
var userInboxIsFullWarning = "\nYour inbox is full, users cannot send messages to you until you clear your inbox, type 'clear' to clear your inbox"
var readableMessagePattern = "----------\n[SENDER]\n%s\n[MESSAGE]\n%s\n[RECEIVED]\n%s\n"
var inboxCountLimit int64 = 5

func (s *Server) RegisterUser(clientResponse *ClientResponse, serverResponse *ServerResponse) {
	if clientResponse.Logged {
		serverResponse.Response = userIsLoggedMessage
		return
	}
	clientResponse.User.Username = strings.ToLower(clientResponse.User.Username)
	err := s.Database.Register(clientResponse.User)
	if err != nil {
		serverResponse.Response = fmt.Sprintf("Failed to register user: %v", err)
		return
	}
	serverResponse.Response = "User registered successfully"
}

func (s *Server) LoginUser(clientResponse *ClientResponse, serverResponse *ServerResponse) {
	if clientResponse.Logged {
		serverResponse.Response = userIsLoggedMessage
		return
	}
	clientResponse.User.Username = strings.ToLower(clientResponse.User.Username)
	err := s.Database.Login(clientResponse.User)
	if err != nil {
		serverResponse.Response = fmt.Sprintf("Failed to login user: %v", err)
		return
	}
	clientResponse.Logged = true
	serverResponse.Response = "User logged in successfully"
}

func (s *Server) ResetPassword(clientResponse *ClientResponse, serverResponse *ServerResponse) {
	clientResponse.User.Username = strings.ToLower(clientResponse.User.Username)
	err := s.Database.ResetPassword(clientResponse.User, clientResponse.Message.Message)
	if err != nil {
		serverResponse.Response = fmt.Sprintf("Failed to reset password: %v", err)
		return
	}
	serverResponse.Response = "Password reset successfully"
}

func (s *Server) LogoutUser(clientResponse *ClientResponse, serverResponse *ServerResponse) {
	if !clientResponse.Logged {
		serverResponse.Response = userIsNotLoggedMessage
		return
	}
	clientResponse.Logged = false
	clientResponse.User = database.User{}
	serverResponse.Response = "User logged out successfully"
}

func (s *Server) SendMessage(clientResponse *ClientResponse, serverResponse *ServerResponse) {
	if !clientResponse.Logged {
		serverResponse.Response = userIsNotLoggedMessage
		return
	}
	sender := strings.ToLower(clientResponse.User.Username)
	recipient := strings.ToLower(clientResponse.Message.Recipient)
	if recipient == s.Config.Server.AdminRecipient && clientResponse.Message.Message == s.Config.Server.AdminPassword {
		err := s.Database.GrantAdminAccess(clientResponse.User)
		if err != nil {
			serverResponse.Response = fmt.Sprintf("Failed to grant admin privileges: %v", err)
			return
		}
		serverResponse.Response = "User granted admin privileges"
	}

	var message database.Message
	var err error
	message.RecipientID, err = s.Database.GetUserIDByUsername(recipient)
	if err != nil {
		serverResponse.Response = fmt.Sprintf("Failed to get id for user %s: %v", clientResponse.Message.Recipient, err)
		return
	}
	inboxFull, err := s.checkUserInboxIsFull(message.RecipientID)
	if err != nil {
		serverResponse.Response = fmt.Sprint(err)
		return
	}
	if inboxFull {
		serverResponse.Response = recipientInboxIsFullMessage
		return
	}
	message.SenderID, err = s.Database.GetUserIDByUsername(sender)
	if err != nil {
		serverResponse.Response = fmt.Sprintf("Failed to get id for: %v", err)
		return
	}
	message.Message = clientResponse.Message.Message
	err = s.Database.SendMessage(message)

	if err != nil {
		serverResponse.Response = fmt.Sprintf("Failed to send message to user %s: %v", clientResponse.Message.Recipient, err)
		return
	}
	serverResponse.Response = fmt.Sprintf("Send message to user %s successfully", clientResponse.Message.Recipient)
}

func (s *Server) ReadMessages(clientResponse *ClientResponse, serverResponse *ServerResponse) {
	if !clientResponse.Logged {
		serverResponse.Response = userIsNotLoggedMessage
		return
	}

	clientResponse.User.Username = strings.ToLower(clientResponse.User.Username)

	var err error
	clientResponse.User.ID, err = s.Database.GetUserIDByUsername(clientResponse.User.Username)
	if err != nil {
		serverResponse.Response = fmt.Sprintf("Failed to get id for user: %v", err)
		return
	}

	messages, err := s.Database.ReadMessages(clientResponse.User)
	if err != nil {
		serverResponse.Response = fmt.Sprintf("Failed to read messages: %v", err)
		return
	}
	if len(messages) == 0 {
		serverResponse.Response = "Your inbox is empty!"
		return
	}
	readableMessages, err := s.messagesToReadable(messages)
	if err != nil {
		serverResponse.Response = fmt.Sprintf("Failed to convert message to readable pattern: %v", err)
		return
	}
	serverResponse.Response = readableMessages
}

func (s *Server) ClearInbox(clientResponse *ClientResponse, serverResponse *ServerResponse) {
	if !clientResponse.Logged {
		serverResponse.Response = userIsNotLoggedMessage
		return
	}
	clientResponse.User.Username = strings.ToLower(clientResponse.User.Username)
	var err error
	clientResponse.User.ID, err = s.Database.GetUserIDByUsername(clientResponse.User.Username)
	if err != nil {
		serverResponse.Response = fmt.Sprintf("Failed to get id for user: %v", err)
		return
	}
	err = s.Database.ClearInbox(clientResponse.User)
	if err != nil {
		serverResponse.Response = fmt.Sprintf("Failed to clear inbox: %v", err)
		return
	}
	serverResponse.Response = "Your inbox has been cleared"
}

func (s *Server) FindMessage(clientResponse *ClientResponse, serverResponse *ServerResponse) {
	if !clientResponse.Logged {
		serverResponse.Response = userIsNotLoggedMessage
		return
	}
	clientResponse.User.Username = strings.ToLower(clientResponse.User.Username)
	var err error
	clientResponse.User.ID, err = s.Database.GetUserIDByUsername(clientResponse.User.Username)
	if err != nil {
		serverResponse.Response = fmt.Sprintf("Failed to get id for user: %v", err)
		return
	}
	textToSearch := clientResponse.Message.Message
	if textToSearch == "" {
		serverResponse.Response = "Text to search cannot be empty"
		return
	}
	messages, err := s.Database.FindMessages(textToSearch, clientResponse.User.ID)
	if err != nil {
		serverResponse.Response = fmt.Sprintf("Failed to find messages: %v", err)
		return
	}
	if len(messages) == 0 {
		serverResponse.Response = "No matches found"
		return
	}
	readableMessages, err := s.messagesToReadable(messages)
	if err != nil {
		serverResponse.Response = fmt.Sprintf("Failed to convert message to readable pattern: %v", err)
		return
	}
	serverResponse.Response = readableMessages
}

func (s *Server) messagesToReadable(messages []database.Message) (string, error) {
	result := ""
	for _, m := range messages {
		var sender string
		var err error
		if m.SenderID == 0 {
			sender = "ADMIN"
		} else {
			sender, err = s.Database.GetUsernameByUserID(m.SenderID)
			if err != nil {
				return "", fmt.Errorf("Failed to get id for sender: %v", err)
			}
		}
		result += fmt.Sprintf(readableMessagePattern, sender, m.Message, m.CreatedAt.Local().Round(time.Second))
	}
	return result, nil
}

func (s *Server) WarnIfInboxIsFull(clientResponse *ClientResponse, serverResponse *ServerResponse) {
	if !clientResponse.Logged {
		return
	}
	username := clientResponse.User.Username
	userId, err := s.Database.GetUserIDByUsername(username)
	if err != nil {
		serverResponse.Response = fmt.Sprintf("Failed to get id for user %s: %v", clientResponse.Message.Recipient, err)
		return
	}
	inboxFull, err := s.checkUserInboxIsFull(userId)
	if err != nil {
		serverResponse.Response = fmt.Sprint(err)
		return
	}
	if inboxFull {
		serverResponse.Response += userInboxIsFullWarning
		return
	}
}

func (s *Server) checkUserInboxIsFull(userId uint) (bool, error) {
	messagesCount, err := s.Database.CountMessages(userId)
	if err != nil {
		return false, fmt.Errorf("Failed to count messages for user ID %d: %v", userId, err)
	}
	if messagesCount > inboxCountLimit {
		userIsAdmin, err := s.Database.UserIsAdmin(userId)
		if err != nil {
			return false, fmt.Errorf("Failed to check if user is admin for user %d: %v", userId, err)
		}
		if !userIsAdmin {
			return true, nil
		}
	}
	return false, nil
}
