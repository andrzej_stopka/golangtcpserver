package server

import (
	"server/database"
)

type ClientResponse struct {
	Command string        `json:"command"`
	User    database.User `json:"user"`
	Logged  bool          `json:"-"`
	Admin   bool          `json:"-"`
	Message Message       `json:"message"`
}

type ServerResponse struct {
	Response string `json:"response"`
}

type Message struct {
	Recipient string `json:"recipient"`
	Message   string `json:"message"`
}
