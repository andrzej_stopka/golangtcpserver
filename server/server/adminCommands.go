package server

import (
	"fmt"
	"server/database"
	"strings"
	"errors"
	"gorm.io/gorm"
)

func (s *Server) SendMessagesToAll(clientResponse *ClientResponse, serverResponse *ServerResponse) {
	if !clientResponse.Logged {
		serverResponse.Response = userIsNotLoggedMessage
		return
	}
	clientResponse.User.Username = strings.ToLower(clientResponse.User.Username)
	if success, reason := s.checkAdminPrivileges(clientResponse.User.Username); !success {
		serverResponse.Response = reason
		return
	}
	var message database.Message
	var err error

	message.SenderID = 0
	message.Message = clientResponse.Message.Message

	err = s.Database.SendMessageToAll(message)
	if err != nil {
		serverResponse.Response = fmt.Sprintf("Failed to send message to all users: %v", err)
		return
	}
	serverResponse.Response = "Message to all users sent successfully"
}

func (s *Server) ReadMessagesFor(clientResponse *ClientResponse, serverResponse *ServerResponse) {
	if !clientResponse.Logged {
		serverResponse.Response = userIsNotLoggedMessage
		return
	}
	clientResponse.User.Username = strings.ToLower(clientResponse.User.Username)
	if success, reason := s.checkAdminPrivileges(clientResponse.User.Username); !success {
		serverResponse.Response = reason
		return
	}
	usernameToCheck := strings.ToLower(clientResponse.Message.Message)
	var err error
	user, err := s.Database.GetUserByUsername(usernameToCheck)
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			serverResponse.Response = fmt.Sprintf("User %s doesn't exist", usernameToCheck)
		} else {
			serverResponse.Response = fmt.Sprintf("Failed to get id for user: %v", err)
		}
		return
	}
	messages, err := s.Database.ReadMessages(user)
	if err != nil {
		serverResponse.Response = fmt.Sprintf("Failed to read messages: %v", err)
		return
	}
	if len(messages) == 0 {
		serverResponse.Response = "User inbox is empty"
		return
	}
	readableMessages, err := s.messagesToReadable(messages)
	if err != nil {
		serverResponse.Response = fmt.Sprintf("Failed to convert message to readable pattern: %v", err)
		return
	}
	serverResponse.Response = readableMessages
}

func (s *Server) DeleteUser(clientResponse *ClientResponse, serverResponse *ServerResponse) {
	if !clientResponse.Logged {
		serverResponse.Response = userIsNotLoggedMessage
		return
	}
	clientResponse.User.Username = strings.ToLower(clientResponse.User.Username)
	if success, reason := s.checkAdminPrivileges(clientResponse.User.Username); !success {
		serverResponse.Response = reason
		return
	}
	usernameToRemove := strings.ToLower(clientResponse.Message.Message)
	var err error
	user, err := s.Database.GetUserByUsername(usernameToRemove)
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			serverResponse.Response = fmt.Sprintf("User %s doesn't exist", usernameToRemove)
		} else {
			serverResponse.Response = fmt.Sprintf("Failed to get id for user: %v", err)
		}
		return
	}
	err = s.Database.DeleteUser(user)
	if err != nil {
		serverResponse.Response = fmt.Sprintf("Failed to delete user %s: %v", usernameToRemove, err)
		return
	}
	serverResponse.Response = "User deleted successfully"
}

func (s *Server) checkAdminPrivileges(username string) (bool, string) {
	userId, err := s.Database.GetUserIDByUsername(username)
	if err != nil {
		reason := fmt.Sprintf("Failed to get id for user %s: %v", username, err)
		return false, reason
	}
	isAdmin, err := s.Database.UserIsAdmin(userId)
	if err != nil {
		reason := fmt.Sprintf("Failed to check admin privileges for user %s: %v", username, err)
		return false, reason
	}
	if !isAdmin {
		reason := fmt.Sprintf("Only admin users can do this!")
		return false, reason
	}
	return true, ""
}
