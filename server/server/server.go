package server

import (
	"encoding/json"
	"io"
	"log"
	"net"
	"server/config"
	"server/database"
	"time"
)

type Server struct {
	Config         *config.Config
	Database       *database.Database
	StartTime      time.Time
	WelcomeMessage string
}

func (s *Server) GetConfig() error {
	var err error
	s.Config, err = config.GetConfig()
	if err != nil {
		return err
	}
	return nil
}

func (s *Server) HandleConnection(conn net.Conn) bool {
	defer conn.Close()

	encoder := json.NewEncoder(conn)
	decoder := json.NewDecoder(conn)
	serverResponse := &ServerResponse{}
	clientResponse := &ClientResponse{}
	for {
		if serverResponse.Response == "" {
			serverResponse.Response = s.WelcomeMessage
		}
		s.WarnIfInboxIsFull(clientResponse, serverResponse)
		err := encoder.Encode(&serverResponse)
		if err != nil {
			log.Printf("Error encoding server response: %v, listening for new connection", err)
			return false
		}
		log.Printf("Sent response: %s to client %s", serverResponse.Response, conn.RemoteAddr())

		err = decoder.Decode(&clientResponse)
		if err != nil {
			if err == io.EOF {
				log.Printf("Connection with %s closed, listening for new connection", conn.RemoteAddr())
				return false
			}
			log.Printf("Error decoding client response, listening for new connection: %v", err)
			return false
		}
		log.Printf("Got command: %s from client %s", clientResponse.Command, conn.RemoteAddr())

		switch clientResponse.Command {
		case "register":
			s.RegisterUser(clientResponse, serverResponse)
		case "reset":
			s.ResetPassword(clientResponse, serverResponse)
		case "login":
			s.LoginUser(clientResponse, serverResponse)
		case "logout":
			s.LogoutUser(clientResponse, serverResponse)
		case "send":
			s.SendMessage(clientResponse, serverResponse)
		case "read":
			s.ReadMessages(clientResponse, serverResponse)
		case "clear":
			s.ClearInbox(clientResponse, serverResponse)
		case "find":
			s.FindMessage(clientResponse, serverResponse)
		case "sendall":
			s.SendMessagesToAll(clientResponse, serverResponse)
		case "readfor":
			s.ReadMessagesFor(clientResponse, serverResponse)
		case "delete":
			s.DeleteUser(clientResponse, serverResponse)
		case "uptime":
			s.GetUptime(serverResponse)
		case "info":
			s.GetInfo(serverResponse)
		case "help":
			s.GetHelp(clientResponse, serverResponse)
		case "stop":
			log.Print("Got stop command, server will be terminated")
			return true
		default:
			serverResponse.Response = "Unknown command"
		}
	}
}
