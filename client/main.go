package main

import (
	"bufio"
	"client/utils"
	"encoding/json"
	"fmt"
	"golang.org/x/term"
	"log"
	"net"
	"os"
	"syscall"
)

var address string = "localhost:8080"

func main() {
	conn, err := net.Dial("tcp", address)
	if err != nil {
		log.Fatalf("error connecting to %s: %v", address, err)
		return
	}
	defer conn.Close()

	encoder := json.NewEncoder(conn)
	decoder := json.NewDecoder(conn)
	var serverResponse utils.ServerResponse
	var clientResponse utils.ClientResponse
	var command string

	scanner := bufio.NewScanner(os.Stdin)

	for {
		err = decoder.Decode(&serverResponse)
		if err != nil {
			log.Fatalf("error decoding server response: %v", err)
			return
		}
		fmt.Printf("\n%s\n", serverResponse.Response)

		fmt.Print("\nType command: ")
		scanner.Scan()
		command = scanner.Text()

		clientResponse.Command = command

		switch command {
		case "register", "login":
			fmt.Print("\nType your username: ")
			scanner.Scan()
			clientResponse.User.Username = scanner.Text()
			fmt.Print("\nType your password: ")
			password, err := term.ReadPassword(int(syscall.Stdin))
			if err != nil {
				fmt.Println("Error reading password:", err)
				return
			}
			fmt.Println()
			clientResponse.User.Password = string(password)

		case "send":
			fmt.Print("\nType recipient username: ")
			scanner.Scan()
			clientResponse.Message.Recipient = scanner.Text()
			fmt.Print("\nType message: ")
			scanner.Scan()
			clientResponse.Message.Message = scanner.Text()

		case "sendall":
			fmt.Print("\nType message: ")
			scanner.Scan()
			clientResponse.Message.Message = scanner.Text()

		case "readfor", "delete":
			fmt.Print("\nType username: ")
			scanner.Scan()
			clientResponse.Message.Message = scanner.Text()

		case "find":
			fmt.Print("\nType text to search: ")
			scanner.Scan()
			clientResponse.Message.Message = scanner.Text()

		case "reset":
			if clientResponse.User.Username == "" {
				fmt.Print("\nType your username: ")
				scanner.Scan()
				clientResponse.User.Username = scanner.Text()
			}
			fmt.Print("\nType your password: ")
			password, err := term.ReadPassword(int(syscall.Stdin))
			if err != nil {
				fmt.Println("Error reading password:", err)
				return
			}
			fmt.Println()
			clientResponse.Message.Message = string(password)

		case "logout":
			clientResponse.User.Username = ""
			clientResponse.User.Password = ""
		}
		
		err = encoder.Encode(&clientResponse)
		if err != nil {
			log.Fatalf("error encoding client response: %v", err)
			return
		}
		if command == "stop" {
			log.Print("Stopping server...")
			return
		}
	}
}
