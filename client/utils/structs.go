package utils

type ClientResponse struct {
	Command string  `json:"command"`
	User    User    `json:"user"`
	Message Message `json:"message"`
}

type ServerResponse struct {
	Response string `json:"response"`
}

type User struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type Message struct {
	Recipient string `json:"recipient"`
	Message   string `json:"message"`
}
